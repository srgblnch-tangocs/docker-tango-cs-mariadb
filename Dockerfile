FROM mariadb:10.6

MAINTAINER TANGO Controls Team <info@tango-controls.org>

# If you change it, remember to modify the dbinit scritps properly
ENV MARIADB_DATABASE=tango
ENV MARIADB_USER=tango
ENV MARIADB_ROOT_PASSWORD=(...)

# allow root access from other machinens in the docker network
# select PASSWORD('(...)') -> *2145A270775CB5D7746A7BEF6242B84D7231A5BB
#CMD echo "grant all privileges on *.* to '$MYSQL_USER'@'192.168.123.11' \
#    identified by password '*2145A270775CB5D7746A7BEF6242B84D7231A5BB' with \
#    grant option;" > script.sql; \
#    mariadb -u root --password=$MYSQL_ROOT_PASSWORD < script.sql

ADD dbinit/create_db.sql \
    dbinit/create_db_tables.sql \
    dbinit/stored_proc.sql \
    dbinit/frontend_privileges.sql \
    /docker-entrypoint-initdb.d/

# TODO: copy the '.in' files and replace variables
#RUN cd /docker-entrypoint-initdb.d; \
#    sed 's/@TANGO_DB_NAME@/'$MARIADB_DATABASE'/g' \
#        create_db.sql.in > create_db.sql ; \
#    sed 's/@SCRIPTS_PATH@/\/docker-entrypoint-initdb.d/g' \
#        create_db.sql.in > create_db.sql ; \
#    mv create_db_tables.sql.in create_db_tables.sql ; \
#    sed 's/@TANGO_DB_NAME@/'$MARIADB_DATABASE'/g' \
#        stored_proc.sql.in > stored_proc.sql ; \
#    mv frontend_privileges.sql.in frontend_privileges.sql
