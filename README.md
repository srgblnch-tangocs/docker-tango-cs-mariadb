# [docker](https://www.docker.com/) [mariadb](https://mariadb.org/) backend for [TANGO Control System](https://www.tango-controls.org/)

![4 - Beta](https://img.shields.io/badge/Development_Status-3_--_alpha-yellow.svg)

## Prepare

See the [docker-compose-tango-cs](https://gitlab.com/srgblnch-tangocs/docker-compose-tango-cs) 
project that uses this.

Docker image based on [MariaDB](https://hub.docker.com/_/mariadb) 
([10.6](https://hub.docker.com/layers/mariadb/library/mariadb/10.6/images/sha256-96b44acb56a0829e7e8a44335576756067c3fe6db619268ec39243d4b488978f?context=explore)) 
and TANGO DB Scheme and backend. It requires the 
[frontend](https://gitlab.com/srgblnch-tangocs/docker-tango-cs).

The files in the directory ```dbinit``` comes from the 
[Tango/DataBase](https://gitlab.com/tango-controls/TangoDatabase) sources and 
they are a little modified.

(TODO) Instead of the hardcoded password use docker secret

```bash
docker build -t tangodb-backend:9.3.4 .
```

## Testing

(TODO) volume tangodatabase:/var/lib/mysql

```bash
docker network create --subnet 192.168.123.0/24 tango
docker run -e --rm -it \
    --network tango \
    -v tangodatabase:/var/lib/mysql \
    --name database \
    tangodb-backend:9.3.4
```

